Feature: Order Placement steps 

Background: 
	Given I open the application 
	
	
	
	
	
@Regression 
Scenario: Verify the header contents of home page 
	Given I am on Home page 
	Then I see company logo 
	And I see store finder 
	And I see login icon 
	And I see wishlist icon 
	And I see cart icon 
	And I see search text field 
	And I see search icon 
	
	
	
@Regression 
Scenario Outline: 
	Checkout: Place Order - credit card payment as Guest user with same shipping and billing address 
	Given I am on Home page 
	And I search for "<product>" and navigate to pdp 
	And I add "<Product>" to the cart 
	And I navigate to shipping page as a guest user 
	And I enter valid "<ShippingAddress>" details in shipping address page 
	And I enter valid "<CardType>" card details in checkout page 
	And I verify the Payment details of "<CardType>" card in ORP 
	And I verify the order summary billing address with "<ShippingAddress>" in ORP 
	And I click on submit order button 
	Then I see order confirmation message 
	Examples: 
		|ShippingAddress|CardType|product|
		|DE|AMEX|qa-test-automation-search-redirect|
		|DE|VISA|qa-test-automation-search-redirect|
		
		
		@Regression 
		Scenario Outline: 
			Checkout: Place Order - Kauf Auf Rechnung payment as Guest user with same shipping and billing address 
			Given I am on Home page 
			When I search for "<product>" and navigate to pdp 
			And I add "<Product>" to the cart 
			And I navigate to shipping page as a guest user 
			And I enter valid "<ShippingAddress>" details in shipping address page 
			And I enter valid "<Payment>" Kauf Auf Rechnung payment details in checkout page 
			And I click on submit order button 
			Then I see order confirmation message 
			Examples: 
				|ShippingAddress|Payment|product|
				|DE|KaufAufRechnung|qa-test-automation-search-redirect|		