package DriverManager;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import utilities.EnvironmentVals;

public class OptionsManager {
	 
    //Get Chrome Options
	
    public ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-popup-blocking");
        System.setProperty("webdriver.chrome.driver", EnvironmentVals.getEnvInstance().getChromeDriver());
        //options.addArguments("--incognito");
        return options;
    }
 
    //Get Firefox Options
    public DesiredCapabilities getFirefoxOptions () {
    	
        /*FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();
        //Accept Untrusted Certificates
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(false);
        //Use No Proxy Settings
        profile.setPreference("network.proxy.type", 0);
        //Set Firefox profile to capabilities
        options.setCapability(FirefoxDriver.PROFILE, profile);
        */
    	DesiredCapabilities caps = DesiredCapabilities.firefox();
		caps.setCapability(CapabilityType.BROWSER_NAME, "firefox");
        caps.setCapability(CapabilityType.PLATFORM_NAME, "WINDOWS");
    	System.setProperty("webdriver.gecko.driver", EnvironmentVals.getEnvInstance().getFirefoxDriver());
        return caps;
    }
    
    public DesiredCapabilities getIEOptions() {
    	DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
		caps.setBrowserName("internet explorer");
		caps.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
        caps.setCapability(CapabilityType.PLATFORM_NAME, "WINDOWS");
		
         //caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		//caps.setCapability("ignoreZoomSetting", true);
		/*caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
		caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);*/
	
		caps.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
		caps.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE, false);
		System.setProperty("webdriver.ie.driver", EnvironmentVals.getEnvInstance().getieDriver());
		return caps;		
    }
    
    public DesiredCapabilities getMobileOptions(String browser) {
    	 /*
		   * https://cs.chromium.org/chromium/src/chrome/test/chromedriver/chrome/mobile_device_list.cc
		   	  iPad
			  Nexus 6
			  Nexus 5
			  Galaxy Note 3
			  Nexus 6P
			  iPhone 8 Plus
			  iPhone 7 Plus
			  Nexus 7
			  iPhone 7
			  Nexus 10
			  iPhone 8
			  iPhone 6
			  Nexus 5X
			  Galaxy Note II
			  iPhone 6 Plus
			  iPhone X
			  Galaxy S5
		   */
    	
    	String mobile = browser.split("_")[1];
    	DesiredCapabilities caps = DesiredCapabilities.chrome();
    	caps.setCapability("platform", "WINDOWS");
    	caps.setBrowserName("chrome");

		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", mobile);
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);

		ChromeOptions options = new ChromeOptions();

		/*if (getCONFIG().getProperty("chached_chrome").equalsIgnoreCase("yes")) {
			options.addArguments("user-data-dir=" + System.getProperty("user.home")
					+ "/AppData/Local/Google/Chrome SxS/User Data/");
			options.addArguments("detach=true");
		}*/

		options.setExperimentalOption("mobileEmulation", mobileEmulation);
		caps.setCapability(ChromeOptions.CAPABILITY, options);	
		return caps;		
    }
    
    
    
    public DesiredCapabilities getBrowserStackCapabilities()
    {
    	/*DesiredCapabilities caps = new DesiredCapabilities();
    	caps.setCapability("browserstack.local", "true");
    	caps.setCapability("browser", "Firefox");
	    caps.setCapability("browser_version", "56.0");
	    caps.setCapability("os", "Windows");
	    caps.setCapability("os_version", "8.1");
	    caps.setCapability("resolution", "1024x768");
        return caps;*/
    	
    	/*DesiredCapabilities caps = new DesiredCapabilities();
    	caps.setCapability("browserName", "android");
        caps.setCapability("device", "Samsung Galaxy S8");
        caps.setCapability("realMobile", "true");
        caps.setCapability("os_version", "7.0");
        caps.setCapability("name", "Bstack-[Java] Sample Test");
        return caps;*/
    	
    	/*DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browserName", "iPhone");
        caps.setCapability("device", "iPhone 8 Plus");
        caps.setCapability("realMobile", "true");
        caps.setCapability("os_version", "11");
        caps.setCapability("name", "Bstack-[Java] Sample Test");
        return caps;*/
    	
    	DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browserName", "iPad");
        caps.setCapability("device", "iPad Pro 9.7 2016");
        caps.setCapability("realMobile", "true");
        caps.setCapability("os_version", "11");
        caps.setCapability("name", "Bstack-[Java] Sample Test");
        return caps;
    }
    
    
}
