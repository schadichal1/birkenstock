package DriverManager;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;



import utilities.EnvironmentVals;
import utilities.FileReaderUtils;

public class TLDriverFactory
{
	private static OptionsManager optionsManager = new OptionsManager();
    private static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<WebDriver>();
    public static String browseris;
	@SuppressWarnings("deprecation")
	public static synchronized void setDriver(String browser) {
		browseris = browser;
    	String strRemoteURL =null;
    	String strBrowserStackURL =null; 
    	String strRemoteExec =  EnvironmentVals.getEnvInstance().getRemoteExecution();
    	String strBrowserStackExec =  EnvironmentVals.getEnvInstance().getBrowserStackExecution();
    	try 
		{	
	    	if(strRemoteExec.equalsIgnoreCase("true"))
	    		strRemoteURL =  EnvironmentVals.getEnvInstance().getRemoteURL();    		
	    	//Below code has to be verified for the correctness yet.
	    	else if(strBrowserStackExec.equalsIgnoreCase("true"))
	    	{
	    		strBrowserStackURL = "https://" + EnvironmentVals.getEnvInstance().getBrowStackUserName() + ":" + EnvironmentVals.getEnvInstance().getBrowStackAccessKey() + "@hub-cloud.browserstack.com/wd/hub";
	    		try {
					tlDriver.set(new RemoteWebDriver(new URL(strBrowserStackURL), optionsManager.getBrowserStackCapabilities()));
				} catch (MalformedURLException e) {				
					e.printStackTrace();
				}
	    	}
	    	else
	    	{
				if (browser.equalsIgnoreCase("firefox"))
				{
		            if (strRemoteExec.equalsIgnoreCase("true"))
		            {
		            	//DesiredCapabilities caps = DesiredCapabilities.firefox();
		            	tlDriver.set(new RemoteWebDriver(new URL(strRemoteURL+"/wd/hub"), optionsManager.getFirefoxOptions()));
		            }
		            
		            else
		            	tlDriver.set(new FirefoxDriver(optionsManager.getFirefoxOptions()));
		        } 
				else if (browser.equals("chrome"))
				{
					if (strRemoteExec.equalsIgnoreCase("true"))
		    			tlDriver.set(new RemoteWebDriver(new URL(strRemoteURL+"/wd/hub"), optionsManager.getChromeOptions()));
					
					else
		            	tlDriver.set(new ChromeDriver(optionsManager.getChromeOptions()));
				}
				else if (browser.equals("internet explorer"))
				{
					if (strRemoteExec.equalsIgnoreCase("true"))
					{
		    			tlDriver.set(new RemoteWebDriver(new URL(strRemoteURL+"/wd/hub"), optionsManager.getIEOptions()));
					}
					
		            else
		            {
		            	System.setProperty("webdriver.ie.driver", EnvironmentVals.getEnvInstance().getieDriver());
		            	tlDriver.set(new InternetExplorerDriver(optionsManager.getIEOptions()));
		            }
				}
				else if (browser.contains("mobile"))
				{
					  /*
					   * https://cs.chromium.org/chromium/src/chrome/test/chromedriver/chrome/mobile_device_list.cc
					   	  iPad
						  Nexus 6
						  Nexus 5
						  Galaxy Note 3
						  Nexus 6P
						  iPhone 8 Plus
						  iPhone 7 Plus
						  Nexus 7
						  iPhone 7
						  Nexus 10
						  iPhone 8
						  iPhone 6
						  Nexus 5X
						  Galaxy Note II
						  iPhone 6 Plus
						  iPhone X
						  Galaxy S5
					   */
					
					System.setProperty("webdriver.chrome.driver", EnvironmentVals.getEnvInstance().getChromeDriver());
	            	tlDriver.set(new ChromeDriver(optionsManager.getMobileOptions(browser)));
				}
		 	}
		}
		catch (MalformedURLException e) 
		{
			Reporter.log("WebDriver instantiation failed");
			e.printStackTrace();
		}
    	
   }
	
    public static synchronized WebDriverWait getWait (WebDriver driver) {
        return new WebDriverWait(driver,20);
    }
 
    public synchronized static WebDriver getDriver() {
        return tlDriver.get();
    }
    
   
}
