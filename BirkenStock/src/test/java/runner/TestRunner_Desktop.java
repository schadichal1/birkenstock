package runner;


import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


//@RunWith(Cucumber.class)
@CucumberOptions(
		features = "Features/DE_OrderPlacementscenarios.feature",
				junit = "--step-notifications",
		glue={"stepDefinitions"},

				plugin = { "pretty",  "junit:target/cucumber-reports/Cucumber.xml",
						"json:target/surefire-reports/Cucumber.json",
							 "html:target/surefire-reports"
						},
	
		tags = {"@Regression"}
		)

public class TestRunner_Desktop extends AbstractTestNGCucumberTests {

	
}
