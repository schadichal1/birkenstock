package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import DriverManager.TLDriverFactory;
import PageObjects.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utilities.EnvironmentVals;

public class HomeStepDefinition {
	WebDriver driver;
	PredefinedSteps predefSteps;
	HomePage homepg;

	public HomeStepDefinition() {
		predefSteps = new PredefinedSteps();
	}

	@Given("^I am on Home page$")
	public void i_am_on_home_page() {
		try {

			driver = TLDriverFactory.getDriver();
			EnvironmentVals.setLocatorProperties("USLocators");
			predefSteps.waitSeconds(2000);
			if (predefSteps.findElementNotPresent(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("cookie_title")))) {
				System.out.println("********* coockie is disp;displaying");
				predefSteps.findByElement(EnvironmentVals.getLocatorProperties()
						.getProperty(predefSteps.formatLocator("confirm_selection_cookies_button"))).click();

			}	
			
			
				
				//Assert.assertTrue(element.isDisplayed(), "Home page load failed");
				predefSteps.waitSeconds(5000);
			
		} catch (Exception E) {
			predefSteps.takeScreenshot("Home page load failure");
			Assert.assertTrue(false, "Home page load has failed due to exception as stated - " + E.getMessage());
		}
	}

	

	@Then("^\"([^\"]*)\" sub category menu displayed$")
	public void hamburger_sub_category_title(String level1CategoryName) {
		String currentCat = predefSteps.findByElement(
				EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("sub_focus_category")))
				.getAttribute("innerText");
		Assert.assertEquals(level1CategoryName.toLowerCase(), currentCat.toLowerCase());
	}



	

	@And("^I search for \"([^\"]*)\" and navigate to (.*)$")

	public void i_search_for(String searchText, String product) {
		try {
			
			 
			predefSteps.typeinEditbox(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("search_text_field")),searchText);
			predefSteps.keyPressEvent("ENTER",EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("search_text_field")));
			/*SearchResultStepDefinition srchPg = new SearchResultStepDefinition();
			srchPg.i_am_on_search_result_page();
			predefSteps.wait(4000);*/
			predefSteps.clickOnElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator(product)));
			predefSteps.waitSeconds(4000);
			Reporter.log("Clicked on " + product);

		} catch (Exception e) {
			System.out.println(e);
		}
		

	}

}
