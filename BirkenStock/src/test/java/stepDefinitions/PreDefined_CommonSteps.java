package stepDefinitions;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import DriverManager.TLDriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utilities.EnvironmentVals;

public class PreDefined_CommonSteps
{
	WebDriver driver;
	PredefinedSteps predefSteps;
	Properties prop;
	HashMap<String, String> hshShippingData;
	//TLDriverFactory diContext;
	String NormalPaypal;
	public PreDefined_CommonSteps()
	{
		this.driver = TLDriverFactory.getDriver();		
		predefSteps= new PredefinedSteps();	
		//this.diContext = diContext;
	}
	
	@And("^I add \"(.*)\" to the cart$")
	public void i_add_item_to_cart(String locator) throws Throwable {
		
				
		try {
			if(predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("width_normal-button"))).isDisplayed()) {
				predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("width_normal-button"))).click();
			}
			else {
				
				predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("width_regular_button"))).click();
				
			}
			
			predefSteps.waitSeconds(3000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("size dropdown"))).click();
			predefSteps.waitSeconds(2000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("size value"))).click();
			predefSteps.waitSeconds(3000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("add to cart button"))).click();
			predefSteps.waitSeconds(3000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("continue to shoppingcart button"))).click();
			predefSteps.waitSeconds(3000);
			
		} catch (Exception e) {
			System.out.println(e);
		} 
		
		
		
	}
	
	@And("^I navigate to shipping page as a guest user$")
	public void navigate_to_shipping_page() throws Throwable {
		EnvironmentVals.setLocatorProperties("USLocators");
		
		try {
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("proceed_to_checkout_button"))).click();
			predefSteps.waitSeconds(1000);
		} catch (Exception e) {
			System.out.println(e);
		}
		
	
		
	}
	
	@And("^I click on (.*)$")
	public void i_click_on(String locator) throws Throwable {
		try {
			
			
			
			
			
			if(predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("summary_accept_terms_checkbox"))).isSelected()||predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("agreement_checkbox"))).isSelected()) {
				predefSteps.waitSeconds(3000);
				predefSteps.clickOnElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator(locator)));
				predefSteps.waitSeconds(3000);
				Reporter.log("Clicked on "+ locator);
			}else {
				predefSteps.waitSeconds(3000);
				predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("summary_accept_terms_checkbox"))).click();
				predefSteps.waitSeconds(3000);
				predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("agreement_checkbox"))).click();
						
				predefSteps.waitSeconds(3000);
				predefSteps.clickOnElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator(locator)));
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
	}
	
	/*@When("^I enter \"(.*)\" in (.*)$")
	public void i_enter_when_no_doublequotes_data_passed(String text, String locator) throws Throwable 
	{
		predefSteps.typeinEditbox(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator(locator)), text);
		Reporter.log(text +" is entered in "+ locator);
	}*/
	
		
	
	
	@Then("^I see (.*)$")
	public void i_should_see_element(String locator)
	{
		
		WebElement element = predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator(locator)));
		Assert.assertTrue(element.isDisplayed(), locator + " is not seen on page");
		Reporter.log(locator +" is verified for its presence on page");
	}
	
	
	
	

	
	
	@When("^wait (\\d+)$")
	public void wait(int time) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   try
	   {
		 Thread.sleep(time);  
	   }
	   catch (Exception E)
	   {
		   Reporter.log(E.getMessage());
	   }
	}
	
	
	
	
	@And("^I enter valid \"(.*)\" card details in checkout page$")
	public void enter_card_details_in_checkout_page(String cardType)
	{
		
		HashMap<String, String> data = predefSteps.readDataFromExcelSheet(cardType, "CardsData");
        predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("creditcard_radio_button"))).click();
		
		predefSteps.waitSeconds(2000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("creditcard_number_field"))).sendKeys(data.get("CardNumber"));
		predefSteps.waitSeconds(1000);
		select_from_dropdown(data.get("ExpMonth"), "expiration_month_selectbox");
		predefSteps.waitSeconds(1000);
		select_from_dropdown(data.get("ExpYear"), "expiration_year_selectbox");
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("security_code_textbox"))).sendKeys(data.get("CVV"));
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("card_holder_name"))).sendKeys(data.get("NameOnCard"));
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("proceed_to_review_button"))).click();
		
		
	}

	
	@And("^I enter valid \"(.*)\" Kauf Auf Rechnung payment details in checkout page$")
	public void enter_Kauf_Auf_Rechnung_payment_details_in_checkout_page(String Payment)
	{
		
		HashMap<String, String> data = predefSteps.readDataFromExcelSheet(Payment, "CardsData");
        predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("invoice_payment_method"))).click();
		
		
		predefSteps.waitSeconds(1000);
		select_from_dropdown(data.get("Birthdate"), "birthdate_dropdown");
		predefSteps.waitSeconds(1000);
		select_from_dropdown(data.get("Month"), "birthmonth_dropdown");
		predefSteps.waitSeconds(1000);
		select_from_dropdown(data.get("Year"), "birthyear_dropdown");
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("proceed_to_review_button"))).click();
		
		
	}
	
	@And("^I enter valid \"(.*)\" details in checkout page$")
	public void enter_paypal_details_in_checkout_page(String NormalPaypal) throws Throwable
	{
		
		
		 WebElement paypal_radio_button = predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("paypal_radio_button")));
		
		System.out.println("*********boolCondition:- "+paypal_radio_button);
		
        if(paypal_radio_button.isSelected()) 
        {
        	
        	predefSteps.waitSeconds(2000);
        	predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("proceed_to_review_button"))).click();
        	
        	predefSteps.waitSeconds(4000);
        	i_click_on("submit_order_button");
        	predefSteps.waitSeconds(2000);
        	paypal_data(NormalPaypal);
        	
    		predefSteps.waitSeconds(3000);
    		
        }
        else {
        	paypal_radio_button.click();
        	predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("proceed_to_review_button"))).click();
        	predefSteps.waitSeconds(5000);
        	 
        
        	
        	 if(predefSteps.findElementNotPresent(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("paypal_login_page")))) 
        	{
        		predefSteps.waitSeconds(5000);
        		paypal_data(NormalPaypal);
        		predefSteps.waitSeconds(4000);
            	i_click_on("submit_order_button");
            	
        	}
        	 else if(predefSteps.findElementNotPresent(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("checkout_details"))))
         	{
         		i_click_on("submit_order_button");
         		predefSteps.waitSeconds(3000);
         		paypal_data(NormalPaypal);
         	}
        	 else {
        	      Reporter.log("No page is displayed..!!");
        	}
        	        	
        	
    		     }
        
	}
	
	
	public void paypal_data(String NormalPaypal) {
		HashMap<String, String> data = predefSteps.readDataFromExcelSheet(NormalPaypal, "CardsData");
		predefSteps.waitSeconds(5000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("email_field"))).clear();
		
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("email_field"))).sendKeys(data.get("LoginUserName"));
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("password_field"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("password_field"))).sendKeys(data.get("LoginPassword"));
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("login_button"))).click();
		predefSteps.waitSeconds(9000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("paypal_continue_button"))).click();
		predefSteps.waitSeconds(7000);
	}
	
	@And("^I verify the Payment details of \"(.*)\" card in ORP$")
	public void verify_payment_details_in_orp(String cardType)
	{
		
		
		HashMap<String, String> data = predefSteps.readDataFromExcelSheet(cardType, "CardsData");
		SoftAssert sa = new SoftAssert();
		/*Assert.assertEquals(predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty("card_name")).getText(), 
				data.get("NameOnCard"),"Verify the card holder name");
		if (cardType.equalsIgnoreCase("VISA")) 
		{
			cardType ="VISA";
		}*/
		
		
		sa.assertEquals(predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty("card_type")).getText(), 
				data.get("NameOnCard"));
		sa.assertEquals(predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty("card_number")).getText().replaceAll("[^0-9]", ""), 
				data.get("CardsLastFourDigit"),"Verify the last four digit of card details");
		sa.assertEquals(predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty("card_expiration_date")).getText(), 
				data.get("ExpectedExpiryDate"));
		sa.assertAll();
		
	}
	
	
	
	@And("^I verify the order summary billing address with \"(.*)\" in ORP$")
	public void verify_billing_address(String address)
	{
		 
		String actualBillingAddress = predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty("billing_address_section")).getText().replace("\n", " ");
		System.out.println("******Actual Billing address:- "+ actualBillingAddress);
		
		if(actualBillingAddress.contains("NY")) 
		{
			HashMap<String, String> data = predefSteps.readDataFromExcelSheet(address, "AddressData");
			String expectedBillingAddress =  data.get("Sections")+" "+data.get("EDITLinks") +" "+ data.get("Salutation")+" "+ data.get("FirstName")+" "+data.get("LastName")
					+" "+data.get("Address1")+" "+
					data.get("City")+", "+data.get("StateCode")+" "+data.get("PostalCode")+" "+
					data.get("Country")+" "+
					data.get("PhoneNumberFormated");	
			System.out.println("********** Expected Billing address:- "+ expectedBillingAddress);
			Assert.assertEquals(actualBillingAddress, expectedBillingAddress,"Verifying the billing address in order confirmation page");
		}
		else 
		{
			HashMap<String, String> data = predefSteps.readDataFromExcelSheet(address, "AddressData");
			String expectedBillingAddress =  data.get("Sections")+" "+data.get("EDITLinks") +" "+ data.get("Salutation")+" "+ data.get("FirstName")+" "+data.get("LastName")
					+" "+data.get("Address1")+" "+data.get("Street Number")+" "+
					data.get("City")+", "+data.get("PostalCode")+" "+
					data.get("Country")+" "+
					data.get("PhoneNumberFormated");	
			System.out.println("********** Expected Billing address:- "+ expectedBillingAddress);
			Assert.assertEquals(actualBillingAddress, expectedBillingAddress,"Verifying the billing address in order confirmation page");
		}
		
		
	}
	
		
	
	
	
	
	@And("^I enter valid \"(.*)\" details in shipping address page$")
	public void enter_shipping_address_in_checkout_page(String shippingAddress)
	{
		
	
		try
		{
			shippingAddress(shippingAddress);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("continue_to_paymentmethod_button"))).click();
			predefSteps.waitSeconds(7000);
			//predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("continue_to_paymentmethod_button"))).click();
		}
		catch(Exception E)
		{
			predefSteps.takeScreenshot("Shipping details");
			Assert.assertTrue(false,"Shipping details entry has failed - "+ E.getMessage());
		} 	
	}
	
	
	@And("^I enter valid \"(.*)\" details in shipping address$")
	public void enter_shipping_address_in_checkout_page1(String shippingAddress)
	{
		
	
		try
		{
			shippingAddress(shippingAddress);
		
		}
		catch(Exception E)
		{
			predefSteps.takeScreenshot("Shipping details");
			Assert.assertTrue(false,"Shipping details entry has failed - "+ E.getMessage());
		} 	
	}
	
	
	
	
	
	
	public void enterShippingAddress(String shippingAddress) {
		EnvironmentVals.setLocatorProperties("USLocators");
		//Enter valid shipping address
		predefSteps.waitSeconds(2000);
		 hshShippingData = predefSteps.readDataFromExcelSheet(shippingAddress, "AddressData");
		
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_first_name"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_first_name"))).sendKeys(hshShippingData.get("FirstName"));
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_last_name"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_last_name"))).sendKeys(hshShippingData.get("LastName"));
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline1"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline1"))).sendKeys(hshShippingData.get("Address1"));
		predefSteps.waitSeconds(1000);
		WebElement value = predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline1")));
 		predefSteps.waitSeconds(2000);
//		value.sendKeys(Keys.ARROW_DOWN);
//		predefSteps.waitSeconds(2000);
//		value.sendKeys(Keys.ARROW_DOWN);
//		
//		value.sendKeys(Keys.ENTER);
 		
	//	predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline2"))).clear();
	//predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline2"))).sendKeys(hshShippingData.get("Address2"));
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_city"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_city"))).sendKeys(hshShippingData.get("City"));
		predefSteps.waitSeconds(1000);
		
		
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_zipcode"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_zipcode"))).sendKeys(hshShippingData.get("PostalCode"));
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_phone"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_phone"))).sendKeys(hshShippingData.get("Phone"));
		predefSteps.waitSeconds(1000);
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_email"))).clear();
		predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_email"))).sendKeys(hshShippingData.get("Email"));
		predefSteps.waitSeconds(3000);
		
		
	}
	
	
	
	public void shippingAddress(String shippingAddress) {
		  
		String locale=EnvironmentVals.getEnvInstance().getConfig().getProperty("Locale").toLowerCase();
		if(locale.equalsIgnoreCase("US")) {
			hshShippingData = predefSteps.readDataFromExcelSheet(shippingAddress, "AddressData");
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_salutation_icon_mr"))).click();
			
			enterShippingAddress(shippingAddress);
			WebElement ele=predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_state")));
			Select state= new Select(ele);
			state.selectByVisibleText(hshShippingData.get("State"));
			
		}else if(locale.equalsIgnoreCase("DE")) {
			EnvironmentVals.setLocatorProperties("DELocators");
			predefSteps.waitSeconds(2000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_salutation_button1"))).click();
			predefSteps.waitSeconds(2000);
			enterShippingAddress(shippingAddress);
			predefSteps.waitSeconds(2000);
			hshShippingData = predefSteps.readDataFromExcelSheet(shippingAddress, "AddressData");
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("street_number"))).sendKeys(hshShippingData.get("Street Number"));
			
			
			predefSteps.waitSeconds(2000);
			
			
			
			
			
		}
	}
	
	
	@And("^I enter valid \"(.*)\" details in billing address form$")
	public void enter_billing_address_in_checkout_page(String billingAddress)
	{
		EnvironmentVals.setLocatorProperties("USLocators");
		//Enter valid shipping address
		predefSteps.waitSeconds(2000);
	
		try
		{
			
			HashMap<String, String> hshShippingData = predefSteps.readDataFromExcelSheet(billingAddress, "AddressData");
			predefSteps.waitSeconds(2000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_salutation"))).click();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_firstname"))).clear();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_firstname"))).sendKeys(hshShippingData.get("FirstName"));
			predefSteps.waitSeconds(1000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_lastname"))).clear();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_lastname"))).sendKeys(hshShippingData.get("LastName"));
			predefSteps.waitSeconds(1000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_address1"))).clear();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_address1"))).sendKeys(hshShippingData.get("Address1"));
			predefSteps.waitSeconds(1000);
			/*WebElement value = predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline1")));
     		predefSteps.waitSeconds(2000);*/
//			value.sendKeys(Keys.ARROW_DOWN);
//			predefSteps.waitSeconds(2000);
//			value.sendKeys(Keys.ARROW_DOWN);
//			
//			value.sendKeys(Keys.ENTER);

			//predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline2"))).clear();
			//predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_addressline2"))).sendKeys(hshShippingData.get("Address2"));
			predefSteps.waitSeconds(1000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_city"))).clear();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_city"))).sendKeys(hshShippingData.get("City"));
			predefSteps.waitSeconds(1000);
			WebElement ele1=predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_state")));
			Select sel1= new Select(ele1);
			sel1.selectByVisibleText(hshShippingData.get("State"));
			predefSteps.waitSeconds(2000);
			
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_zipcode"))).clear();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_zipcode"))).sendKeys(hshShippingData.get("PostalCode"));
			predefSteps.waitSeconds(2000);
			//WebElement ele2=predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("shipping_country")));
			//Select sel2= new Select(ele2);
			//sel2.selectByVisibleText(hshShippingData.get("Country"));
			predefSteps.waitSeconds(2000);
			
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_phonenumber"))).clear();
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("billing_phonenumber"))).sendKeys(hshShippingData.get("Phone"));
			predefSteps.waitSeconds(3000);
			
			
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("continue_to_paymentmethod_button"))).click();
			predefSteps.waitSeconds(10000);
			predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator("continue_to_paymentmethod_button"))).click();
		}
		catch(Exception E)
		{
			predefSteps.takeScreenshot("Shipping details");
			Assert.assertTrue(false,"Shipping details entry has failed - "+ E.getMessage());
		} 	
	}
	
		
	@Given("^I open the application$")
	public void i_open_application()
	{
		Reporter.log("*********************************************************");
		predefSteps.navigate_to_env_url();
		//EnvironmentVals.setLocatorProperties("USLocators");
	}
	
	
		
	@When("^I select \"([^\']*)\" from (.*)$")
	public void select_from_dropdown(String option, String locator) 
	{
		try
		{
			WebElement dropdownlist = predefSteps.findByElement(EnvironmentVals.getLocatorProperties().getProperty(predefSteps.formatLocator(locator)));
	       
			  if(dropdownlist.getTagName().equalsIgnoreCase("Select"))
			  {
				  Select select = new Select(dropdownlist);
		          select.selectByVisibleText(option);
		          Reporter.log("Selected " + option + " from locator");
			  }
			  else
			  {
				  dropdownlist.click();
				  List<WebElement> listItems = dropdownlist.findElements(By.xpath(".//ul/li"));
				 
				  for(WebElement item:listItems)
				  {
					  if(item.getAttribute("innerText").equalsIgnoreCase(option) || item.getAttribute("value").equalsIgnoreCase(option))
					  {
						  item.click();
						  break;
					  }
				  }
				 
			  }
		}
		catch(Exception e)
		{
			predefSteps.takeScreenshot("select from listbox failed");
			Assert.assertTrue(false,"Select from listbox failed due to exception - "+ e.getMessage());
		}
	}
	
		
}
