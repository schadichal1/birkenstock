package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import DriverManager.TLDriverFactory;
import utilities.EnvironmentVals;
import utilities.FileReaderUtils;

public class PredefinedSteps
{
	WebDriver driver;
	public HashMap<String, String> dataRow;
	
	public PredefinedSteps()
	{
		this.driver = TLDriverFactory.getDriver();
	}
	
	public WebDriver open_new_browser()
	{
		String browserName = EnvironmentVals.getEnvInstance().getBrowser();
		try
		{
			if (browserName.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", EnvironmentVals.getEnvInstance().getFirefoxDriver());
				driver = new FirefoxDriver();
			}
			else if (browserName.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", EnvironmentVals.getEnvInstance().getChromeDriver());
				driver = new ChromeDriver();
			} 
			else if (browserName.equalsIgnoreCase("internet explorer")) {
				//Not yet implemented IE driver
			} 
			else {
				System.err.println("Invalid browser! Unable to execute...");
				return null;
			}
			
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			Reporter.log("<br>Webdriver initialised successfully.");
			//Reporter.log("<br>Browser Info: " + caps.getBrowserName() + " ver "	+ caps.getVersion());
			
		}
		catch (Exception E)
		{
			Reporter.log("Launch Browser :: " + " exception = " + E.getMessage()+ " Line Number = "	+ Thread.currentThread().getStackTrace()[3].getLineNumber());
		}
		Reporter.log("Open Browser Completed- " + browserName +" browser is open");
		return driver;
	}
	
	
	public void navigate_to_env_url()
	{
		try
		{
			driver.manage().deleteAllCookies();
			driver.get(EnvironmentVals.getEnvInstance().getEnvURL());
			
		}
		catch(Exception e)
		{
			Reporter.log("Open application has failed." + e.getMessage());
		}
	}
	
	
	
	public  WebElement findByElement(String locatorPath)
	{
		
		WebElement elm = null;
		By locate = null;
		String arr[] = locatorPath.split(",", 2);
		String identiFyBy = arr[0];
		String locator = arr[1];
				
		try
		{
			if(identiFyBy.equalsIgnoreCase("xpath"))
				  locate = By.xpath(locator);
			else if(identiFyBy.equalsIgnoreCase("id"))
				 locate = By.id(locator);
			else if(identiFyBy.equalsIgnoreCase("name"))
				 locate = By.name(locator);
			else if(identiFyBy.equalsIgnoreCase("css"))
				 locate = By.cssSelector(locator);
			else if(identiFyBy.equalsIgnoreCase("className"))
				 locate = By.className(locator);
			else if(identiFyBy.equalsIgnoreCase("tagName"))
				 locate = By.tagName(locator);
			else if(identiFyBy.equalsIgnoreCase("linkText"))
				 locate = By.linkText(locator);
			else if(identiFyBy.equalsIgnoreCase("partialLinkText"))
				 locate = By.partialLinkText(locator);
			
			//elm = getDriver().findElement(locate);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			return wait.until(ExpectedConditions.presenceOfElementLocated(locate));
			//elm = driver.findElement(locate);
		}
		catch (Exception e)
		{
			//logs.debug(MessageFormat.format(LoggingMsg.FORMATTED_ERROR_MSG, e.getMessage()));
			takeScreenshot("WebElementDoesNotExists");
			Assert.assertTrue(false,"Test is Failed due to exception occured in the method findByElement for the locator "	+ locator + "  due to  " + e.getMessage());
			return null;
		} 
		//return elm;					
	}
	
	public Boolean findElementNotPresent(String locatorPath)
	{
		boolean flag;
		WebElement elm = null;
		By locate = null;
		String arr[] = locatorPath.split(",", 2);
		String identiFyBy = arr[0];
		String locator = arr[1];
				
		try
		{
			if(identiFyBy.equalsIgnoreCase("xpath"))
				  locate = By.xpath(locator);
			else if(identiFyBy.equalsIgnoreCase("id"))
				 locate = By.id(locator);
			else if(identiFyBy.equalsIgnoreCase("name"))
				 locate = By.name(locator);
			else if(identiFyBy.equalsIgnoreCase("css"))
				 locate = By.cssSelector(locator);
			else if(identiFyBy.equalsIgnoreCase("className"))
				 locate = By.className(locator);
			else if(identiFyBy.equalsIgnoreCase("tagName"))
				 locate = By.tagName(locator);
			else if(identiFyBy.equalsIgnoreCase("linkText"))
				 locate = By.linkText(locator);
			else if(identiFyBy.equalsIgnoreCase("partialLinkText"))
				 locate = By.partialLinkText(locator);
			
			elm = driver.findElement(locate);
			flag = true;
		}
		catch (Exception e)
		{
			//takeScreenshot("WebElementDoesNotExists");
			//Assert.assertTrue(true,"Test is Failed due to exception occured in the method Find element not present for the locator "	+ locator + "  due to  " + e.getMessage());
			flag = false;
		}
	
		return flag;
	}
    
    public  List<WebElement> findByElements(String locatorPath)
	{
		java.util.List<WebElement> elms = null;
		By locate = null;
		String arr[] = locatorPath.split(",", 2);
		String identiFyBy = arr[0];
		String locator = arr[1];
		try
		{
			if(identiFyBy.equalsIgnoreCase("xpath"))
				  locate = By.xpath(locator);
			else if(identiFyBy.equalsIgnoreCase("id"))
				 locate = By.id(locator);
			else if(identiFyBy.equalsIgnoreCase("name"))
				 locate = By.name(locator);
			else if(identiFyBy.equalsIgnoreCase("css"))
				 locate = By.cssSelector(locator);
			else if(identiFyBy.equalsIgnoreCase("className"))
				 locate = By.className(locator);
			else if(identiFyBy.equalsIgnoreCase("tagName"))
				 locate = By.tagName(locator);
			else if(identiFyBy.equalsIgnoreCase("linkText"))
				 locate = By.linkText(locator);
			else if(identiFyBy.equalsIgnoreCase("partialLinkText"))
				 locate = By.partialLinkText(locator);
		
			//elms = getDriver().findElements(locate);
				WebDriverWait wait = new WebDriverWait(driver, 60);
				return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locate));
				
			//	elms = driver.findElements(locate);

			}
			catch (Exception e)
			{
				takeScreenshot("WebElementDoesNotExists");
				Assert.assertTrue(false,"Test is Failed due to exception occured in the method findByElements for the locator "	+ locator + "  due to  " + e.getMessage());
				return null;
			} 			
		//	return elms;				
	}
    
    public  void clickOnElement(String locatorPath) {
		
    	try {
			if(!locatorPath.isEmpty())
			{
				WebElement web_Element = findByElement(locatorPath);
				if(web_Element != null)
					{web_Element.click(); Thread.sleep(2000);}
					
				else
					Reporter.log("Click on "+ locatorPath+ " has failed as the element was not found");
			}
			
		} catch (Exception e) {
			takeScreenshot("WebElementDoesNotExists");
			Assert.assertTrue(false,"Test is Failed due to exception occured in the method click on element for the locator "	+ locatorPath + "  due to  " + e.getMessage());
		}	
	}
	
   
    
	public  void typeinEditbox(String locatorPath, String valuetoType) 
	{
		try {
			  WebElement web_Element = findByElement(locatorPath);
			  web_Element.clear();
			  web_Element.sendKeys(Keys.chord(Keys.CONTROL,"a",Keys.DELETE));

			  if(!valuetoType.isEmpty()) {
			   web_Element.sendKeys(valuetoType);
			   web_Element.sendKeys(Keys.TAB);
			  }
			 } 
			catch (Exception e) {
				takeScreenshot("WebElementDoesNotExists");
				Assert.assertTrue(false,"Test is Failed due to exception occured in the method typeinEditbox on element "	+ locatorPath + "  due to  " + e.getMessage());
			 }  

	}
	
	public void selectFromDropDown(String locatorPath, String selectLocator , String valuetoSelect)
	{
		try {
			WebElement web_Element = findByElement(locatorPath);
			web_Element.click();
			Thread.sleep(2000);
			WebElement elm_options = findByElement(selectLocator.replaceFirst("<aaa>", valuetoSelect));
			elm_options.click();
		} catch (Exception e) {
			takeScreenshot("WebElementDoesNotExists");
			Assert.assertTrue(false,"Test is Failed due to exception occured in the method selectFromDropDown on element for the locator "	+ locatorPath + "  due to  " + e.getMessage());
		}
	}
	
	public void waitSeconds(long sec)
	{
		try {
			Thread.sleep(sec);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public HashMap<String, String> readDataFromExcelSheet(String rowReference, String SheetName)
	{
		
		FileReaderUtils fls = new FileReaderUtils();
		dataRow = fls.ReadDataFromRow(EnvironmentVals.getEnvInstance().getTestDataPath(), SheetName, rowReference);
		if(dataRow != null)
			return dataRow;
		else
		{
			Reporter.log("Specified data row could not be reached.");
			return null;
		}
	}
	
	public void closeBrowserDriver()
	{
		driver.close();
		//driver.quit();
	}

	public void takeScreenshot(String testNo) {
		String strScreenshotsFolder = System.getProperty("ScreenshotsFolder");
		if (strScreenshotsFolder == null) strScreenshotsFolder = System.getProperty("user.dir")+"//Screenshots";
		try {
			waitSeconds(1500);
			DateFormat dateFormat = new SimpleDateFormat("yyyy_dd_MMM_HH_mm_ss");
			Date date = new Date();
			String getCurrentDate = dateFormat.format(date);

			WebDriver augmentedDriver = new Augmenter().augment(driver);
			File scrFile = ((TakesScreenshot) augmentedDriver)
					.getScreenshotAs(OutputType.FILE);
			String path = strScreenshotsFolder + "/" + testNo + "_"
					+ getCurrentDate + ".png";

			FileUtils.copyFile(scrFile, new File(path));
			System.setProperty("org.uncommons.reportng.escape-output", "false");
			FileUtils.copyFile(scrFile, new File(path));
			new File(strScreenshotsFolder).mkdirs();
			Reporter.log("To View Screenshot <a href=\"file:///" + path
					+ "\">Click Here</a>");
		} catch (IOException e) {
			Reporter.log("Exception in the method takeScreenshot: "	+ e.getMessage());
			Assert.assertTrue(false, " Exception in the method takeScreenshot:  "+ e.getMessage());
		}
	}
	
	public String formatLocator(String locator)
	{
		String locatorString;
		 if(TLDriverFactory.browseris.contains("mobile"))
		      locatorString=locator.concat("_mobile").replaceAll(" ", "_").toLowerCase();
		 else
		      locatorString=locator.replaceAll(" ", "_").toLowerCase();
		 
		 return locatorString;
	}
	
	public void keyPressEvent(String strKey, String locatorPath)
	{
		try
		{
		WebElement	element = findByElement(locatorPath);
		
		switch(strKey.toUpperCase()){
             case "TAB":
            	 element.sendKeys(Keys.TAB);
            	 break;
             case "BACKSPACE":
            	 element.sendKeys(Keys.BACK_SPACE);
            	 break;
             case "CLEAR":
            	 element.sendKeys(Keys.CLEAR);
            	 break;
             case "CANCEL":
            	 element.sendKeys(Keys.CANCEL);
            	 break;
             case "DELETE":
            	 element.sendKeys(Keys.DELETE);
            	 break;
             case "ENTER":
            	 element.sendKeys(Keys.ENTER);
            	 break;
			}
		}
		catch (Exception e) {
			takeScreenshot("WebElementDoesNotExists");
			Assert.assertTrue(false,"Test is Failed due to exception occured in the method keyPressEvent on element for the locator "	+ locatorPath + "  due to  " + e.getMessage());
		}
	}
	
	public String readAttribute(String locatorPath, String attributeType)
	{
		try
		{
		WebElement web_Element = findByElement(locatorPath);
		
        String attribute = web_Element.getAttribute(attributeType);
        return attribute;
		}
		catch(Exception e)
		{
			Reporter.log("Reading attribute of the locator failed " + e.getMessage());
			return null;
		}
			
      
	}


	
}
