package stepDefinitions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import DriverManager.TLDriverFactory;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utilities.EnvironmentVals;

public class SearchResultStepDefinition 
{
	WebDriver driver ;
	PredefinedSteps predefSteps;
	
	public SearchResultStepDefinition()
	{
		predefSteps= new PredefinedSteps();	
	}

	@Given("^I am on search result page$")
	public void i_am_on_search_result_page()
	{
		driver = TLDriverFactory.getDriver();
		EnvironmentVals.setLocatorProperties("USLocators");
		
	}
	
	
}
