package utilities;

import java.util.Properties;

public class EnvironmentVals {
	
	 
	public static Properties prop = null;
	private String configPropFilePath = System.getProperty("user.dir")+"//src//test//java//config//config.properties";
	private String locatorsPath = System.getProperty("user.dir")+"//src//test//java//selectors//";
	public static EnvironmentVals globalVals = new EnvironmentVals();
	static FileReaderUtils flReader;
	private static String locatorsFilePath;
    
	public EnvironmentVals()
	{
	}
	
	public static EnvironmentVals getEnvInstance()
	{
	
		return globalVals;		
	}
	
 	
	
	public Properties getConfig()
	{
		flReader = new FileReaderUtils();
		prop =  flReader.ReadFromTextFile(configPropFilePath);
		return prop;
	}
	
	public String getChromeDriver()
	{
		String driverPath = getConfig().getProperty("chromeDriver");	
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Chrome driverPath is not specified in the config.properties file.");		

	}
	
	public String getFirefoxDriver()
	{
		String driverPath = getConfig().getProperty("firefoxDriver");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Firefox driverPath is not specified in the config.properties file.");
	}
	public String getieDriver()
	{
		String driverPath = getConfig().getProperty("ieDriver");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("IE driverPath is not specified in the config.properties file.");
	}
	
	public String getEnvURL()
	{
		String locale = getConfig().getProperty("Locale").toLowerCase();
		String envURL = getConfig().getProperty(locale +"_"+"testEnvironment");		
		if(envURL != null) return envURL;
		else throw new RuntimeException("Test environment URL not specified in the config.properties file.");
	}
	
	public String getBrowser()
	{
		String browser = getConfig().getProperty("Browser");		
		if(browser != null) return browser;
		else throw new RuntimeException("Browser is not specified in the config.properties file.");
	}
	
	public String getRemoteExecution()
	{
		String remoteExec = getConfig().getProperty("RemoteExecution");	
		if(remoteExec!= null) return remoteExec;
		else throw new RuntimeException("Remote Execution is not specified in the config.properties file.");		

	}
	
	public String getRemoteURL()
	{
		String remoteURL = getConfig().getProperty("RemoteURL");	
		if(remoteURL != null) return remoteURL;
		else throw new RuntimeException("Remote URL is not specified in the config.properties file.");		

	}
	
	public String getBrowserStackExecution()
	{
		String browserStack = getConfig().getProperty("BrowserStackExecution");	
		if(browserStack != null) return browserStack;
		else throw new RuntimeException("Browser Stack Execution value is not specified in the config.properties file.");	
	}
	
	public String getBrowStackUserName()
	{
		String userName = getConfig().getProperty("UserName");	
		if(userName != null) return userName;
		else throw new RuntimeException("Browser Stack UserName value is not specified in the config.properties file.");	
	}

	public String getBrowStackAccessKey()
	{
		String accessKey = getConfig().getProperty("AccessKey");	
		if(accessKey != null) return accessKey;
		else throw new RuntimeException("Browser Stack AccessKey value is not specified in the config.properties file.");	
	}
	
	public String getTestDataPath()
	{
		String locale =getConfig().getProperty("Locale").toLowerCase();
		String testDataFilePath = getConfig().getProperty(locale+"_"+"testData");	
		if(testDataFilePath != null) return testDataFilePath;
		else throw new RuntimeException("Test Data file path is not specified in config.properties file.");	
	}
	
	public String getLocatorFilePath(String locFileName)
	{
		String locale = getConfig().getProperty("Locale").toLowerCase();
		locatorsFilePath = getConfig().getProperty(locale+"_"+"selectors");
		//prop = flReader.ReadFromTextFile(locatorsFilePath);
		return locatorsFilePath;
	}
		
	 public static void setLocatorProperties(String locFileName)
    {	
    	locatorsFilePath = EnvironmentVals.getEnvInstance().getLocatorFilePath(locFileName);	
    	//System.out.println("locatorsFilePath:" + locatorsFilePath);
	}
	    
    public static Properties getLocatorProperties()
    {	
    	
    	prop = flReader.ReadFromTextFile(locatorsFilePath);
		return prop;
    }
	
}
