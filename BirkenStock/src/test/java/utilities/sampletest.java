package utilities;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import DriverManager.TLDriverFactory;
import stepDefinitions.PredefinedSteps;


public class sampletest  { //extends PredefinedSteps{

	WebDriver driver;
	@Test
	public void run() 
	{
		PredefinedSteps preps = new PredefinedSteps();
		preps.open_new_browser();
		driver = TLDriverFactory.getDriver();
		
		preps.navigate_to_env_url();
		
		Reporter.log("Navigated to given environment url");
		preps.waitSeconds(3000);
		
		
	}
}
