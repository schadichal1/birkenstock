$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Features/US_OrderPlacementscenarios.feature");
formatter.feature({
  "line": 1,
  "name": "Order Placement steps",
  "description": "",
  "id": "order-placement-steps",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open the application",
  "keyword": "Given "
});
formatter.match({
  "location": "PreDefined_CommonSteps.i_open_application()"
});
formatter.result({
  "duration": 15850558053,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Verify the header contents of home page",
  "description": "",
  "id": "order-placement-steps;verify-the-header-contents-of-home-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "I am on Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see company logo",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I see store finder",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I see login icon",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I see wishlist icon",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I see cart icon",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I see search text field",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I see search icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomeStepDefinition.i_am_on_home_page()"
});
formatter.result({
  "duration": 7587743321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "company logo",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 134760903,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "store finder",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 49840757,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "login icon",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 41558433,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "wishlist icon",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 39389678,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cart icon",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 45465316,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "search text field",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 44592013,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "search icon",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 33830794,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 23,
  "name": "Checkout: Place Order - credit card payment as Guest user with same shipping and billing address",
  "description": "",
  "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 22,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "I am on Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "I search for \"\u003cproduct\u003e\" and navigate to pdp",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "I add \"\u003cProduct\u003e\" to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I navigate to shipping page as a guest user",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I enter valid \"\u003cShippingAddress\u003e\" details in shipping address page",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I enter valid \"\u003cCardType\u003e\" card details in checkout page",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I verify the Payment details of \"\u003cCardType\u003e\" card in ORP",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I verify the order summary billing address with \"\u003cShippingAddress\u003e\" in ORP",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I click on submit order button",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I see order confirmation message",
  "keyword": "Then "
});
formatter.examples({
  "line": 34,
  "name": "",
  "description": "",
  "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address;",
  "rows": [
    {
      "cells": [
        "ShippingAddress",
        "CardType",
        "product"
      ],
      "line": 35,
      "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address;;1"
    },
    {
      "cells": [
        "US",
        "AMEX",
        "qa-test-automation-search-redirect"
      ],
      "line": 36,
      "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address;;2"
    },
    {
      "cells": [
        "US",
        "Discover (only US)",
        "qa-test-automation-search-redirect"
      ],
      "line": 37,
      "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open the application",
  "keyword": "Given "
});
formatter.match({
  "location": "PreDefined_CommonSteps.i_open_application()"
});
formatter.result({
  "duration": 10558138923,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Checkout: Place Order - credit card payment as Guest user with same shipping and billing address",
  "description": "",
  "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 22,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "I am on Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "I search for \"qa-test-automation-search-redirect\" and navigate to pdp",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "I add \"\u003cProduct\u003e\" to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I navigate to shipping page as a guest user",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I enter valid \"US\" details in shipping address page",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I enter valid \"AMEX\" card details in checkout page",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I verify the Payment details of \"AMEX\" card in ORP",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I verify the order summary billing address with \"US\" in ORP",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I click on submit order button",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I see order confirmation message",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeStepDefinition.i_am_on_home_page()"
});
formatter.result({
  "duration": 8097460102,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "qa-test-automation-search-redirect",
      "offset": 14
    },
    {
      "val": "pdp",
      "offset": 66
    }
  ],
  "location": "HomeStepDefinition.i_search_for(String,String)"
});
formatter.result({
  "duration": 53371956213,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cProduct\u003e",
      "offset": 7
    }
  ],
  "location": "PreDefined_CommonSteps.i_add_item_to_cart(String)"
});
formatter.result({
  "duration": 15128577275,
  "status": "passed"
});
formatter.match({
  "location": "PreDefined_CommonSteps.navigate_to_shipping_page()"
});
formatter.result({
  "duration": 9143743388,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "US",
      "offset": 15
    }
  ],
  "location": "PreDefined_CommonSteps.enter_shipping_address_in_checkout_page(String)"
});
formatter.result({
  "duration": 37304680629,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AMEX",
      "offset": 15
    }
  ],
  "location": "PreDefined_CommonSteps.enter_card_details_in_checkout_page(String)"
});
formatter.result({
  "duration": 11859477615,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AMEX",
      "offset": 33
    }
  ],
  "location": "PreDefined_CommonSteps.verify_payment_details_in_orp(String)"
});
formatter.result({
  "duration": 382051633,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "US",
      "offset": 49
    }
  ],
  "location": "PreDefined_CommonSteps.verify_billing_address(String)"
});
formatter.result({
  "duration": 177949474,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "submit order button",
      "offset": 11
    }
  ],
  "location": "PreDefined_CommonSteps.i_click_on(String)"
});
formatter.result({
  "duration": 20124747146,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "order confirmation message",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 123297739,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open the application",
  "keyword": "Given "
});
formatter.match({
  "location": "PreDefined_CommonSteps.i_open_application()"
});
formatter.result({
  "duration": 11459284749,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Checkout: Place Order - credit card payment as Guest user with same shipping and billing address",
  "description": "",
  "id": "order-placement-steps;checkout:-place-order---credit-card-payment-as-guest-user-with-same-shipping-and-billing-address;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 22,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "I am on Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "I search for \"qa-test-automation-search-redirect\" and navigate to pdp",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "I add \"\u003cProduct\u003e\" to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I navigate to shipping page as a guest user",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I enter valid \"US\" details in shipping address page",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I enter valid \"Discover (only US)\" card details in checkout page",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I verify the Payment details of \"Discover (only US)\" card in ORP",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I verify the order summary billing address with \"US\" in ORP",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I click on submit order button",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "I see order confirmation message",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeStepDefinition.i_am_on_home_page()"
});
formatter.result({
  "duration": 7036898734,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "qa-test-automation-search-redirect",
      "offset": 14
    },
    {
      "val": "pdp",
      "offset": 66
    }
  ],
  "location": "HomeStepDefinition.i_search_for(String,String)"
});
formatter.result({
  "duration": 23886962792,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cProduct\u003e",
      "offset": 7
    }
  ],
  "location": "PreDefined_CommonSteps.i_add_item_to_cart(String)"
});
formatter.result({
  "duration": 15159325847,
  "status": "passed"
});
formatter.match({
  "location": "PreDefined_CommonSteps.navigate_to_shipping_page()"
});
formatter.result({
  "duration": 4872483898,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "US",
      "offset": 15
    }
  ],
  "location": "PreDefined_CommonSteps.enter_shipping_address_in_checkout_page(String)"
});
formatter.result({
  "duration": 34380164905,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Discover (only US)",
      "offset": 15
    }
  ],
  "location": "PreDefined_CommonSteps.enter_card_details_in_checkout_page(String)"
});
formatter.result({
  "duration": 9872276840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Discover (only US)",
      "offset": 33
    }
  ],
  "location": "PreDefined_CommonSteps.verify_payment_details_in_orp(String)"
});
formatter.result({
  "duration": 168303869,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "US",
      "offset": 49
    }
  ],
  "location": "PreDefined_CommonSteps.verify_billing_address(String)"
});
formatter.result({
  "duration": 86885413,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "submit order button",
      "offset": 11
    }
  ],
  "location": "PreDefined_CommonSteps.i_click_on(String)"
});
formatter.result({
  "duration": 16942320488,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "order confirmation message",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "duration": 63806023,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 43,
  "name": "",
  "description": "Checkout: Place Order - Normal PayPal as Guest user with same shipping and billing address",
  "id": "order-placement-steps;",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 42,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 45,
  "name": "I am on Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 46,
  "name": "I search for \"\u003cproduct\u003e\" and navigate to pdp",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "I add \"\u003cProduct\u003e\" to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "I navigate to shipping page as a guest user",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "I enter valid \"\u003cShippingAddress\u003e\" details in shipping address page",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "I enter valid \"\u003cPaypal\u003e\" details in checkout page",
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 51,
      "value": "#And I verify the order summary billing address with \"\u003cShippingAddress\u003e\" in ORP"
    }
  ],
  "line": 52,
  "name": "I see order confirmation message",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "I see paypal text",
  "keyword": "And "
});
formatter.examples({
  "line": 54,
  "name": "",
  "description": "",
  "id": "order-placement-steps;;",
  "rows": [
    {
      "cells": [
        "ShippingAddress",
        "Paypal",
        "product"
      ],
      "line": 55,
      "id": "order-placement-steps;;;1"
    },
    {
      "cells": [
        "US",
        "NormalPaypal",
        "qa-test-automation-search-redirect"
      ],
      "line": 56,
      "id": "order-placement-steps;;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open the application",
  "keyword": "Given "
});
formatter.match({
  "location": "PreDefined_CommonSteps.i_open_application()"
});
formatter.result({
  "duration": 8800541028,
  "status": "passed"
});
formatter.scenario({
  "line": 56,
  "name": "",
  "description": "Checkout: Place Order - Normal PayPal as Guest user with same shipping and billing address",
  "id": "order-placement-steps;;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 42,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 45,
  "name": "I am on Home page",
  "keyword": "Given "
});
formatter.step({
  "line": 46,
  "name": "I search for \"qa-test-automation-search-redirect\" and navigate to pdp",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "I add \"\u003cProduct\u003e\" to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "I navigate to shipping page as a guest user",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "I enter valid \"US\" details in shipping address page",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "I enter valid \"NormalPaypal\" details in checkout page",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "comments": [
    {
      "line": 51,
      "value": "#And I verify the order summary billing address with \"\u003cShippingAddress\u003e\" in ORP"
    }
  ],
  "line": 52,
  "name": "I see order confirmation message",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "I see paypal text",
  "keyword": "And "
});
formatter.match({
  "location": "HomeStepDefinition.i_am_on_home_page()"
});
formatter.result({
  "duration": 7047464051,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "qa-test-automation-search-redirect",
      "offset": 14
    },
    {
      "val": "pdp",
      "offset": 66
    }
  ],
  "location": "HomeStepDefinition.i_search_for(String,String)"
});
formatter.result({
  "duration": 70017935515,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cProduct\u003e",
      "offset": 7
    }
  ],
  "location": "PreDefined_CommonSteps.i_add_item_to_cart(String)"
});
formatter.result({
  "duration": 35266276261,
  "status": "passed"
});
formatter.match({
  "location": "PreDefined_CommonSteps.navigate_to_shipping_page()"
});
formatter.result({
  "duration": 7247494664,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "US",
      "offset": 15
    }
  ],
  "location": "PreDefined_CommonSteps.enter_shipping_address_in_checkout_page(String)"
});
formatter.result({
  "duration": 63223063594,
  "error_message": "java.lang.AssertionError: Test is Failed due to exception occured in the method findByElement for the locator //div[contains(@class,\u0027button-radio\u0027)]//label[@class\u003d\u0027button button-radio\u0027]  due to  Expected condition failed: waiting for presence of element located by: By.xpath: //div[contains(@class,\u0027button-radio\u0027)]//label[@class\u003d\u0027button button-radio\u0027] (tried for 60 second(s) with 500 milliseconds interval) expected [true] but found [false]\r\n\tat org.testng.Assert.fail(Assert.java:94)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:513)\r\n\tat org.testng.Assert.assertTrue(Assert.java:42)\r\n\tat stepDefinitions.PredefinedSteps.findByElement(PredefinedSteps.java:131)\r\n\tat stepDefinitions.PreDefined_CommonSteps.shippingAddress(PreDefined_CommonSteps.java:421)\r\n\tat stepDefinitions.PreDefined_CommonSteps.enter_shipping_address_in_checkout_page(PreDefined_CommonSteps.java:335)\r\n\tat ✽.And I enter valid \"US\" details in shipping address page(Features/US_OrderPlacementscenarios.feature:49)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "NormalPaypal",
      "offset": 15
    }
  ],
  "location": "PreDefined_CommonSteps.enter_paypal_details_in_checkout_page(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "order confirmation message",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "paypal text",
      "offset": 6
    }
  ],
  "location": "PreDefined_CommonSteps.i_should_see_element(String)"
});
formatter.result({
  "status": "skipped"
});
});